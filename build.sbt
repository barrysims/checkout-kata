name := "Checkout kata Java 8"

version := "1.0"

organization := "com.barrysims"

scalaVersion := "2.11.0"

scalacOptions ++= Seq("-unchecked", "-feature", "-deprecation")

libraryDependencies ++= Seq(
  "org.scalatest" % "scalatest_2.11" % "2.2.1" % "test",
  "org.functionaljava" % "functionaljava" % "4.2-beta-1",
  "org.functionaljava" % "functionaljava-java8" % "4.2-beta-1",
  "org.scalaz" %% "scalaz-core" % "7.1.0")