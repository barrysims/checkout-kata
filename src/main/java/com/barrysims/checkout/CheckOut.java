package com.barrysims.checkout;

import fj.Ord;
import fj.data.List;
import fj.data.Option;
import static fj.Monoid.intAdditionMonoid;

public class CheckOut {

    /**
     * Find the cheapest set of prices by expanding the tree of matching rules
     *
     * Recursively apply rules to the list of SKUs
     *
     * @return The cheapest price
     */
    public Option<Integer> calculate(List<PriceRule> rules, List<String> skus) {
        return calc(rules, skus, Option.some(0));
    }

    private Option<Integer> calc(List<PriceRule> rules, List<String> skus, Option<Integer> total) {

        if (skus.isEmpty()) return total;

        else {
            List<PriceRule> applicableRules = rules.filter(r -> r.applicable(skus));

            List<Option<Integer>> appliedRules = applicableRules.map(
                    r -> calc(rules, r.applyRule(skus), sumPrices(List.<Option<Integer>>nil().cons(Option.some(r.price)).cons(total)))
            );

            List<Option<Integer>> filteredRules = appliedRules.filter(Option::isSome);

            if (filteredRules.isEmpty()) return Option.none();

            else return Option.some(filteredRules.map(r -> r.some()).minimum(Ord.intOrd));
        }
    }

    private Option<Integer> sumPrices(List<Option<Integer>> prices) {
        return sequence(prices).map(intAdditionMonoid::sumLeft);
    }

    private Option<List<Integer>> sequence(List<Option<Integer>> lo) {
        if (lo.exists(Option::isNone)) { return Option.none(); }
        else return Option.some(lo.map(o -> o.some()));
    }
}