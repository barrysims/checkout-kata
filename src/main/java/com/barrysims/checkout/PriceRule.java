package com.barrysims.checkout;

import fj.data.List;

/**
 * PriceRule models a general price rule
 */
abstract public class PriceRule {

    int price;

    /**
     * Determines if this rule is applicable to a list of SKUs
     *
     * @param skuList The list of SKUs
     * @return True if the rule can be used
     */
    abstract Boolean applicable(final List<String> skuList);

    /**
     * Applies a price rule to a list of SKUs
     *
     * @param skuList The list of SKUs
     * @return The SKU list after the price rule has been applied
     */
    abstract List<String> applyRule(final List<String> skuList);
}
