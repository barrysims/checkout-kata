package com.barrysims.checkout;

import fj.Equal;
import fj.data.List;

/**
 * ListUtil adds a couple of functions not available in the fj.data.List implementation
 */
public class ListUtil {

    /**
     * Remove one list from another
     *
     * @param list1 Start list
     * @param list2 list to be removed
     * @return the remainder of list1 once list2's items removed
     */
    public static List<String> remove(final List<String> list1, final List<String> list2) {
        if (list2.isEmpty()) return list1;
        else return remove(list1.delete(list2.head(), Equal.stringEqual), list2.tail());
    }

    /**
     * Check that a list contains all members of another list (is a superset of the other list)
     *
     * @param list1 The list we are checking
     * @param list2 The members we are searching for in list1
     * @return True if list1 contains all members of list2 (in sufficient quantities)
     */
    public static Boolean contains(final List<String> list1, final List<String> list2) {
        if (list2.isEmpty()) return true;
        else {
            Boolean inList = !(list1.delete(list2.head(), Equal.stringEqual).equals(list1));
            return inList && contains(list1.tail(), list2.delete(list1.head(), Equal.stringEqual));
        }
    }

}
