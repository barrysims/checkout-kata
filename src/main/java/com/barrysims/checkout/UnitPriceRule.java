package com.barrysims.checkout;

import fj.data.List;

import static fj.data.List.list;

/**
 * UnitPriceRule models a simple price for a single item
 */
public class UnitPriceRule extends PriceRule {

    /**
     * the SKU this price rule refers to
     */
    private final String sku;

    public UnitPriceRule(final String sku, int price) {
        this.sku = sku;
        this.price = price;
    }

    @Override
    final List<String> applyRule(final List<String> skuList) {
        return ListUtil.remove(skuList, list(sku));
    }

    @Override
    final Boolean applicable(final List<String> skuList) {
        return ListUtil.contains(skuList, list(sku));
    }

    @Override
    final public String toString() {
        return "\n" + sku + " @ " + price;
    }
}
