package com.barrysims.checkout;

import fj.data.List;

/**
 * MultibuyPriceRule models a pricing offer in which multiples of items can be purchased at a discount
 */
public class MultibuyPriceRule extends PriceRule {

    /**
     * the list of items this deal refers to
     */
    private final List<String> skuList;

    public MultibuyPriceRule(final List<String> skuList, int price) {
        this.skuList = skuList;
        this.price = price;
    }

    @Override
    List<String> applyRule(List<String> checkOutSkuList) {
        return ListUtil.remove(checkOutSkuList, skuList);
    }

    @Override
    final Boolean applicable(final List<String> checkOutSkuList) {
        return ListUtil.contains(checkOutSkuList, skuList);
    }

    @Override
    final public String toString() {
        return "\n" + skuList + " @ " + price;
    }
}
