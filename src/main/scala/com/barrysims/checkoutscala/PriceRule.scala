package com.barrysims.checkoutscala

import scala.annotation.tailrec

/**
 */
trait PriceRule {

  /**
   * Determines if this rule is applicable to a list of SKUs
   *
   * @param skuList The list of SKUs
   * @return True if the rule can be used
   */
  def applicable(skuList: List[Sku]): Boolean

  /**
   * Applies a price rule to a list of SKUs
   *
   * @param skuList The list of SKUs
   * @return The SKU list after the price rule has been applied
   */
  def applyRule(skuList: List[Sku]): List[Sku]

  def price: Int
}

/**
 * UnitPriceRule models a simple price for a single item
 */
case class UnitPriceRule(sku: Sku, price: Int) extends PriceRule {

  def applyRule(skuList: List[Sku]): List[Sku] = skuList.diff(List(sku))
  def applicable(checkOutSkuList: List[Sku]): Boolean = checkOutSkuList.contains(sku)
  override def toString: String = s"\n$sku @ $price"
}

/**
 * MultibuyPriceRule models a pricing offer in which multiples of items can be purchased at a discount
 */
case class MultibuyPriceRule(skuList: List[Sku], price: Int) extends PriceRule {

  def applyRule(checkOutSkuList: List[Sku]): List[Sku] = checkOutSkuList.diff(skuList)
  def applicable(checkOutSkuList: List[Sku]): Boolean = checkOutSkuList.hasAllOf(skuList)
  override def toString: String = s"\n$skuList @ $price"
}
