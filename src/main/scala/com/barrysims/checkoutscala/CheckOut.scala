package com.barrysims.checkoutscala

import scalaz._
import Scalaz._

object CheckOut {

  /**
   * Find the cheapest set of prices by expanding the tree of matching rules
   *
   * Recursively apply rules to the list of SKUs
   *
   * @return The cheapest price
   */
  def apply(rules: List[PriceRule], skus: List[Sku]): Option[Int] = calc(rules, skus, Some(0))

  private def calc(rules: List[PriceRule], skus: List[Sku], total: Option[Int]): Option[Int] = skus match {

    case Nil => total

    case s => rules.collect {
      case r if r.applicable(s) => calc(rules, r.applyRule(s), sumPrices(total :: Some(r.price) :: Nil))
    }.flatten match {
      case Nil => None
      case cs => Some(cs.min)
    }
  }

  private def sumPrices(prices: List[Option[Int]]) = prices.sequence.map(_.sum)
}