package com.barrysims

import scala.language.implicitConversions

package object checkoutscala {
  type Sku = String
  implicit def listToListUtil[A](list: List[A]): ListUtil[A] = ListUtil[A](list)
  implicit def listUtilToList[A](listUtil: ListUtil[A]): List[A] = listUtil.as
}

case class ListUtil[A](as: List[A]) {
  def hasAllOf(bs: List[A]): Boolean = as.diff(bs).length == as.length - bs.length
}
