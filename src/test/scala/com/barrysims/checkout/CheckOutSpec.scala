package com.barrysims.checkout

import org.scalatest.{Matchers, FlatSpec}

import fj.data.List.list
import fj.data.Option

/**
 * User: Barry
 * Date: 26/05/13
 */
class CheckOutSpec extends FlatSpec with Matchers {

    val checkOut = new CheckOut()

    "CheckOut" should "calculate the correct total for a single item" in {
        checkOut.calculate(
          list(new UnitPriceRule("A", 10)),
          list("A")
        ) should equal(Option.some(10))
    }

    it should "calculate the correct total for a list of items" in {
        checkOut.calculate(
          list(new UnitPriceRule("A", 10)),
          list("A", "A", "A")
        ) should equal(Option.some(30))
    }

    it should "calculate the correct total for mixed items" in {
        checkOut.calculate(
          list(new UnitPriceRule("A", 10), new UnitPriceRule("B", 15)),
          list("A", "B")
        ) should equal(Option.some(25))
    }

    it should "fail with an exception when no pricing rule exists for an item" in {
        checkOut.calculate(
          list(new UnitPriceRule("B", 10)),
          list("A")
        ) should equal(Option.none())
    }

    it should "calculate the correct total for a multi-buy item" in {
        checkOut.calculate(
          list(new MultibuyPriceRule(list("A", "A"), 15)),
          list("A", "A")
        ) should equal(Option.some(15))
    }

    it should "calculate the correct total for a multi-buy and unit combination" in {
        checkOut.calculate(
          list(new MultibuyPriceRule(list("A", "A"), 15), new UnitPriceRule("A", 10)),
          list("A", "A", "A")
        ) should equal(Option.some(25))
    }

    it should "calculate the correct total for mixed multi-buy and unit combinations" in {
        checkOut.calculate(
          list(
            new MultibuyPriceRule(list("A", "A"), 15),
            new UnitPriceRule("A", 10),
            new UnitPriceRule("B", 20)),
          list("A", "A", "A", "B")
        ) should equal(Option.some(45))
    }

    it should "calculate the correct total for mixed multi-buy combinations" in {
        checkOut.calculate(
          list(
            new MultibuyPriceRule(list("A", "A"), 15),
            new UnitPriceRule("A", 10),
            new MultibuyPriceRule(list("B", "B", "B"), 50),
            new UnitPriceRule("B", 20)),
          list("A", "A", "A", "B", "B", "B", "B")
        ) should equal(Option.some(95))
    }

  it should "calculate the correct total for mixed multi-buy combinations with no unit-buys" in {
    checkOut.calculate(
      list(
        new MultibuyPriceRule(list("B", "B", "A"), 20),
        new MultibuyPriceRule(list("A", "A"), 15)),
        list("A", "A", "A", "A", "B", "B", "B", "B")
    ) should equal(Option.some(55))
  }

  it should "calculate the correct total for pricerule combinations that can result in dead ends" in {
    checkOut.calculate(
      list(
        new MultibuyPriceRule(list("B", "B", "A"), 20),
        new MultibuyPriceRule(list("A", "A"), 15)),
      list("A", "A", "A", "A", "B", "B", "B", "B")
    ) should equal(Option.some(55))
  }


  it should "handle cases where not all items can be purchased" in {
        checkOut.calculate(
          list(new MultibuyPriceRule(list("A", "A"), 15)),
          list("A", "A", "A")
        ) should equal(Option.none())
   }

}
