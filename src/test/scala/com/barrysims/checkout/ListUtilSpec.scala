package com.barrysims.checkout

import org.scalatest.{Matchers, FlatSpec}

import fj.data.List.list

/**
 * User: Barry
 * Date: 27/05/13
 */
class ListUtilSpec extends FlatSpec with Matchers {

    "ListUtil#remove" should "remove all items in one list from another" in {
        val list1 = list("A", "B", "C")
        val list2 = list("A", "B")
        ListUtil.remove(list1, list2) should equal(list("C"))
    }

    it should "remove all items in one list from another with repeated items" in {
        val list1 = list("A", "B", "C", "C", "C")
        val list2 = list("C", "C")
        ListUtil.remove(list1, list2) should equal(list("A", "B", "C"))
    }

    "ListUtil#contains" should "correctly check if one list contains another" in {
        val list1 = list("A", "B", "C")
        val list2 = list("A", "B")
        ListUtil.contains(list1, list2) should equal(true)
    }

    it should "correctly check if one list doesn't contain another" in {
        val list1 = list("A", "B", "C")
        val list2 = list("A", "D")
        ListUtil.contains(list1, list2) should equal(false)
    }

    it should "correctly check if one list contains another with repeated items" in {
        val list1 = list("A", "B", "C", "C", "C")
        val list2 = list("A", "C", "C")
        ListUtil.contains(list1, list2) should equal(true)
    }

    it should "correctly check if one list doesn't contain another with repeated items" in {
        val list1 = list("A", "B", "C", "C", "C")
        val list2 = list("A", "C", "C", "B", "B")
        ListUtil.contains(list1, list2) should equal(false)
    }

}