package com.barrysims.checkoutscala

import org.scalatest.{FlatSpec, Matchers}

/**
 *
 */
class CheckOutSpecScala extends FlatSpec with Matchers {

  "contains" should "work as expected" in {
    List("A", "C", "B").hasAllOf(List("B", "A")) should equal(true)
  }

  "diff" should "work as expected" in {
    List("A", "A", "A").diff(List("A", "A")) should equal(List("A"))
  }

  "CheckOut" should "calculate the correct total for a single item" in {
    CheckOut (
      List(UnitPriceRule("A", 10)),
      List("A")
    ) should equal(Some(10))
  }

  it should "calculate the correct total for a list of items" in {
    CheckOut (
      List(UnitPriceRule("A", 10)),
      List("A", "A", "A")
    ) should equal(Some(30))
  }

  it should "calculate the correct total for mixed items" in {
    CheckOut (
      List(UnitPriceRule("A", 10), UnitPriceRule("B", 15)),
      List("A", "B")
    ) should equal(Some(25))
  }

  it should "fail with an exception when no pricing rule exists for an item" in {
    intercept[RuntimeException] {
      CheckOut (
        List(UnitPriceRule("B", 10)),
        List("A")
      ) should equal(Some(10))
    }
  }

  it should "calculate the correct total for a multi-buy item" in {
    CheckOut (
      List(MultibuyPriceRule(List("A", "A"), 15)),
      List("A", "A")
    ) should equal(Some(15))
  }

  it should "calculate the correct total for a multi-buy and unit combination" in {
    CheckOut (
      List(MultibuyPriceRule(List("A", "A"), 15), UnitPriceRule("A", 10)),
      List("A", "A", "A")
    ) should equal(Some(25))
  }

  it should "calculate the correct total for mixed multi-buy and unit combinations" in {
    CheckOut (
      List(
        MultibuyPriceRule(List("A", "A"), 15),
        UnitPriceRule("A", 10),
        UnitPriceRule("B", 20)),
      List("A", "A", "A", "B")
    ) should equal(Some(45))
  }

  it should "calculate the correct total for mixed multi-buy combinations" in {
    CheckOut (
      List(
        MultibuyPriceRule(List("A", "A"), 15),
        UnitPriceRule("A", 10),
        MultibuyPriceRule(List("B", "B", "B"), 50),
        UnitPriceRule("B", 20)),
      List("A", "A", "A", "B", "B", "B", "B")
    ) should equal(Some(95))
  }

  it should "calculate the correct total for pricerule combinations that can result in dead ends" in {
    CheckOut (
      List(
        MultibuyPriceRule(List("B", "B", "A"), 20),
        MultibuyPriceRule(List("A", "A"), 15)),
      List("A", "A", "A", "A", "B", "B", "B", "B")
    ) should equal(Some(55))
  }

  it should "calculate the correct total for pricerule combinations that can result in dead ends 2" in {
    CheckOut (
      List(
        MultibuyPriceRule(List("A", "A"), 15),
        MultibuyPriceRule(List("B", "B", "A"), 20)),
      List("A", "A", "A", "A", "B", "B", "B", "B")
    ) should equal(Some(55))
  }

  it should "handle cases where not all items can be purchased" in {
    CheckOut (
      List(MultibuyPriceRule(List("A", "A"), 15)),
      List("A", "A", "A")
    ) should equal (None)
  }
}
