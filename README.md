Checkout Kata
=============

A very general solution, decoupling the pricing rules from the checkout.  New types of price rule can be added by extending the abstract price rule class / trait.

The algorithm to find the best price expands the solution tree of applicable price rules, selecting the cheapest branch.

----------

Two solutions: Java8 with the Functional Java library, and Scala.
